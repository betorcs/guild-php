<?php

namespace Guild\Tests;

use PHPUnit\Framework\TestCase;

class ScrappyTest extends TestCase
{

    public function testHello()
    {
        $text = "Hello";

        $this->assertNotNull($text);
        $this->assertEquals("Hello", $text);
    }

    public function testNull() {
        $text = null;

        $this->assertNull($text);
    }

    public function tetNumber() {
        $number = "34";

        $this->assertIsNumeric($number);
    }

    public function testException()
    {
        $this->expectException(\Exception::class);

        throw new \Exception("Blah!");    
    }

    public function testExceptionMessage() 
    {
        $this->expectExceptionMessage("Blah!");

        throw new \Exception("Blah!");    
    }

    /**
     * @dataProvider sumProvider
     */
    public function testSum($a, $b, $expected) {
        $sum = $a + $b;
        $this->assertEquals($expected, $sum);
    }

    public function sumProvider() {
        return [
            [1, 1, 2],
            [1, 3, 4],
            [2, 7, 9],
            [12, 3, 15]
        ];
    }
}
