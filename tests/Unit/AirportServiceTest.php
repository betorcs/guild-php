<?php

namespace Guild\Tests;

use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;

use Guild\AirportService;
use Guild\Tests\Mocks\MockAirportProvider;
use Guild\Tests\Mocks\MockTimeProvider;

class AirportServiceTest extends TestCase
{

    protected $format = 'Y-m-d H:i:s';
    protected $now = '2020-02-27 10:15:00';

    /** @var DateTime */
    protected $givenDateTime;

    /** @var MockTimeProvider */
    protected $timeProvider;

    /** @var AirportService */
    protected $airportService;

    protected function setUp(): void
    {
        $airportProvider = new MockAirportProvider();
        $this->timeProvider = new MockTimeProvider();
        $this->airportService = new AirportService($airportProvider, $this->timeProvider);

        $this->givenDateTime = DateTime::createFromFormat($this->format, $this->now, new DateTimeZone('America/Sao_Paulo'));
    }

    public function testGetTimeInAirport()
    {
        // Given
        $expected = '2020-02-27 11:15:00';
        $iata = 'FEN';

        // Prepare
        $this->timeProvider->setDateTime($this->givenDateTime);

        // When
        $dateTime = $this->airportService->getDateTimeInAirport($iata);

        // Then
        $this->assertNotNull($dateTime);
        $this->assertEquals($dateTime->format($this->format), $expected);
    }

    /**
     * @dataProvider iataDateTimeProvider
     */
    public function testGetTimeInAirports($iata, $expected)
    {
        // Prepare
        $this->timeProvider->setDateTime($this->givenDateTime);

        // When
        $dateTime = $this->airportService->getDateTimeInAirport($iata);

        // Then
        $this->assertNotNull($dateTime);
        $this->assertEquals($dateTime->format($this->format), $expected);
    }

    public function iataDateTimeProvider()
    {
        return [
            ['FEN', '2020-02-27 11:15:00'],
            ['VCP', '2020-02-27 10:15:00'],
            ['LIS', '2020-02-27 13:15:00']
        ];
    }
}
