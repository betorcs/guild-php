<?php

namespace Guild\Tests\Mocks;

use Guild\Providers\AirportProvider;
use Guild\Exceptions\AirportNotFoundException;
use Guild\Model\Airport;

class MockAirportProvider implements AirportProvider {

    /**
     * @inheritdoc
     */
    public function getAirportByIata($iata) {
        if ($iata == 'FEN') {
            $airport = new Airport();
            $airport->tz = 'America/Noronha';
            return $airport;
        } else if ($iata = 'VPC') {
            $airport = new Airport();
            $airport->tz = 'America/Sao_Paulo';
            return $airport;
        } else if ($iata = 'MCO') {
            $airport = new Airport();
            $airport->tz = 'America/New_York';
            return $airport;
        } else if ($iata = 'LIS') {
            $airport = new Airport();
            $airport->tz = 'Europe/Lisbon';
            return $airport;
        } 
        throw new AirportNotFoundException($iata);
    }

}