<?php

namespace Guild\Tests\Mocks;

use DateTime;
use Guild\Providers\TimeProvider;

class MockTimeProvider implements TimeProvider {

    /** @var DateTime */
    private $dateTime;

    public function __construct(DateTime $dateTime = null)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param DateTime $dateTime
     */
    public function setDateTime(DateTime $dateTime) {
        $this->dateTime = $dateTime;
    }

    /**
     * @inheritdoc
     */
    public function getDateTime() {
        return $this->dateTime;
    }

}