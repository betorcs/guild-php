<?php

namespace Guild\Tests;

use Guild\Providers\NetworkAirportProvider;
use PHPUnit\Framework\TestCase;

class NetworkAirportProviderIntegrationTest extends TestCase {

    
    public function testGetAirportByIata() {

        $apiBaseUrl = 'https://ws.icrew.app';
        $apiToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJHMlpURnFHSG5KUkVNTmJFZjh5YSIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.DMOWDGi-Gtvetzy_ylvBk6DbubQZdDmJcIkyZje3xyI';
        
        /** @var NetworkAirportProvider */
        $provider = new NetworkAirportProvider($apiBaseUrl, $apiToken);

        // Given
        $iata = 'FEN';

        // When
        $airport = $provider->getAirportByIata($iata);

        // Then
        $this->assertNotNull($airport);
        $this->assertEquals($airport->name, 'Fernando de Noronha Airport');
        $this->assertEquals($airport->tz, 'America/Noronha');
    }


}