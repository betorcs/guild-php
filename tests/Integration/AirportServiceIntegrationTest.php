<?php

use Guild\AirportService;
use Guild\Providers\DefaultTimeProvider;
use Guild\Providers\NetworkAirportProvider;
use PHPUnit\Framework\TestCase;

class AirportServiceIntegrationTest extends TestCase {

    protected $format = 'Y-m-d H:i:s';
    
    /** @var AirportService */
    protected $airportService;

    protected function setUp(): void
    {
        $apiBaseUrl = 'https://ws.icrew.app';
        $apiToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJHMlpURnFHSG5KUkVNTmJFZjh5YSIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.DMOWDGi-Gtvetzy_ylvBk6DbubQZdDmJcIkyZje3xyI';

        $airportProvider = new NetworkAirportProvider($apiBaseUrl, $apiToken);
        $timeProvider = new DefaultTimeProvider();
        $this->airportService = new AirportService($airportProvider, $timeProvider);
    }


    /**
     * @dataProvider iataDateTimeProvider
     */
    public function testGetTimeInAirports($iata, $expected)
    {
        // When
        $dateTime = $this->airportService->getDateTimeInAirport($iata);

        // Then
        $this->assertNotNull($dateTime);
        $this->assertEquals($expected, $dateTime->getTimezone()->getName());
    }

    public function iataDateTimeProvider()
    {
        return [
            ['FEN', 'America/Noronha' ],
            ['VCP', 'America/Sao_Paulo' ],
            ['LIS', 'Europe/Lisbon' ]
        ];
    }

}