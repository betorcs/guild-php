<?php

namespace Guild;

use DateTime;
use DateTimeZone;
use Guild\Providers\AirportProvider;
use Guild\Providers\TimeProvider;

class AirportService
{

    /**
     * @var AirportProvider
     */
    private $airportProvider;

    /** @var TimeProvider */
    private $timeProvider;

    function __construct(AirportProvider $airportProvider, TimeProvider $timeProvider)
    {
        $this->airportProvider = $airportProvider;
        $this->timeProvider = $timeProvider;
    }

    /**
     * @return DateTime
     */
    public function getDateTimeInAirport($iata) {
        $airport = $this->airportProvider->getAirportByIata($iata);
        $now = $this->timeProvider->getDateTime();
        $now->setTimezone(new DateTimeZone($airport->tz));
        return $now;
    }
}
