<?php 

namespace Guild\Providers;

use DateTime;
use DateTimeZone;

class DefaultTimeProvider implements TimeProvider {

    /**
     * @inheritdoc
     */
    public function getDateTime() {
        return new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
    }

}
