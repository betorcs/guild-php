<?php

namespace Guild\Providers;

use Guild\Model\Airport;

interface AirportProvider {

    /**
     * @param string $iata - Código aeroportuário.
     * @return Airport - Dados do aeroporto.
     */
    function getAirportByIata($iata);

}