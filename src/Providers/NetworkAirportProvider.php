<?php

namespace Guild\Providers;

use Guild\Model\Airport;
use GuzzleHttp;

class NetworkAirportProvider implements AirportProvider {

    private $apiBaseUrl;
    private $apiToken;

    function __construct(string $apiBaseUrl, string $apiToken)
    {
        $this->apiBaseUrl = $apiBaseUrl;
        $this->apiToken = $apiToken;
    }
 
    /**
     * @inheritdoc
     */
    function getAirportByIata($iata) {
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', $this->apiBaseUrl . '/api/airport/' . $iata, [
            'headers' => [
                'x-api' => $this->apiToken
            ]
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('The API returned status code ' . $response->getStatusCode());
        }

        $body = $response->getBody();
        
        return Airport::fromJson($body);
    }

}