<?php

namespace Guild\Providers;

use DateTime;

interface TimeProvider {

    /**
     * @return DateTime
     */
    public function getDateTime();

}