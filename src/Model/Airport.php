<?php

namespace Guild\Model;

class Airport
{
    public $airportid;
    public $name;
    public $city;
    public $country;
    public $iata;
    public $icao;
    public $latitude;
    public $longitude;
    public $altitude;
    public $dst;
    public $tz;
    public $type;
    public $source;

    /**
     * @param String $stringJSON
     * @return Airport
     */
    static function fromJson($stringJSON) {
        $json = json_decode($stringJSON);
        $airport = new Airport();
        foreach ($json as $key => $value) {
            $airport->$key = $value;
        }
        return $airport;
    }
}
