<?php

namespace Guild\Exceptions;

class AirportNotFoundException extends \Exception {

    function __construct($iata)
    {
        parent::__construct('Nenhum aeroport encontrado com código: ' . $iata);
    }
}